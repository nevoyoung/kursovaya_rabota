from pymongo import MongoClient
from product import Product


# Класс, отвечающий за взаимодействие с базой данных MongoDB
class MongoDBManager:
    def __init__(self):
        self.client = MongoClient("mongodb://localhost:27017/")
        self.db = self.client["productdb"]
        self.collection = self.db["products"]

    def addProduct(self, product):
        self.collection.insert_one({"name": product.name, "quantity": product.quantity})

    def removeProduct(self, productName):
        result = self.collection.delete_one({"name": productName})
        if result.deleted_count > 0:
            print("Товар успешно удален из базы данных.")
        else:
            print("Товар не найден в базе данных.")

    def getAllProducts(self):
        products = []
        for doc in self.collection.find():
            product = Product(doc["name"], doc["quantity"])
            products.append(product)
        return products