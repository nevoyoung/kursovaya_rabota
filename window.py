from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPalette, QColor
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QPushButton, QListWidget, QMessageBox
from product import Product
from product import PharmacyWarehouse
from database import MongoDBManager

# Класс, представляющий главное окно приложения
class MainWindow(QMainWindow):
    def __init__(self, warehouse, dbManager):
        super().__init__()
        self.warehouse = warehouse
        self.dbManager = dbManager

        self.setWindowTitle("Аптечный склад")

        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)

        self.layout = QVBoxLayout(self.central_widget)

        self.label = QLabel("Товары на складе:")
        self.layout.addWidget(self.label)

        self.list_widget = QListWidget()
        self.populateList()
        self.layout.addWidget(self.list_widget)

        self.form_layout = QHBoxLayout()

        # Изменяем цвет фона на светло-серый
        pal = self.centralWidget().palette()
        pal.setColor(QPalette.Background, QColor(208, 217, 255))
        self.centralWidget().setAutoFillBackground(True)
        self.centralWidget().setPalette(pal)

        # Запрещаем изменение размера окна
        self.setFixedWidth(480)
        self.setFixedHeight(400)
        self.setWindowFlags(Qt.WindowMinimizeButtonHint | Qt.WindowCloseButtonHint)

        # Создаем стиль для кнопок
        remove_button_style = "background-color:red;color:white;"
        add_button_style = "background-color:green;color:white;"
        view_button_style = "background-color:blue;color:white;"

        self.product_label = QLabel("Название:")
        self.form_layout.addWidget(self.product_label)

        self.product_lineedit = QLineEdit()
        self.form_layout.addWidget(self.product_lineedit)

        self.quantity_label = QLabel("Количество:")
        self.form_layout.addWidget(self.quantity_label)

        self.quantity_lineedit = QLineEdit()
        self.form_layout.addWidget(self.quantity_lineedit)

        self.add_button = QPushButton("Добавить товар")
        self.add_button.setStyleSheet(add_button_style)
        self.add_button.clicked.connect(self.addProduct)
        self.form_layout.addWidget(self.add_button)

        self.remove_button = QPushButton("Удалить товар")
        self.remove_button.setStyleSheet(remove_button_style)
        self.remove_button.clicked.connect(self.removeProduct)
        self.form_layout.addWidget(self.remove_button)

        self.layout.addLayout(self.form_layout)

    def populateList(self):
        self.list_widget.clear()
        products = self.warehouse.getProducts()
        if not products:
            products = self.dbManager.getAllProducts()
        for product in products:
            self.list_widget.addItem(f"Товар: {product.name} | Кол-во: {product.quantity}")

    def addProduct(self):
        product_name = self.product_lineedit.text()
        quantity = self.quantity_lineedit.text()
        product = Product(product_name, quantity)
        self.warehouse.addProduct(product)
        self.dbManager.addProduct(product)
        self.populateList()
        self.product_lineedit.clear()
        self.quantity_lineedit.clear()

    def removeProduct(self):
        selected_item = self.list_widget.currentItem()
        if selected_item:
            product_info = selected_item.text().split(" | ")
            product_name = product_info[0].split(": ")[1]
            self.warehouse.removeProduct(product_name)
            self.dbManager.removeProduct(product_name)
            self.list_widget.takeItem(self.list_widget.row(selected_item))
        else:
            QMessageBox.warning(self, "Ошибка", "Выберите товар для удаления.")

if __name__=="__main__":
    # Создаем объекты склада и менеджера базы данных
    warehouse = PharmacyWarehouse()
    dbManager = MongoDBManager()

    # Создаем приложение и главное окно
    app = QApplication([])
    main_window = MainWindow(warehouse, dbManager)
    main_window.show()

    # Запускаем приложение
    app.exec()