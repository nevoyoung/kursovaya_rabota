import threading

# Класс, представляющий товар на складе аптеки
class Product:
    def __init__(self, name, quantity):
        self.name = name
        self.quantity = quantity

# Класс, представляющий склад аптеки
class PharmacyWarehouse:
    def __init__(self):
        self.products = []
        self.lock = threading.Lock()

    def addProduct(self, product):
        with self.lock:
            self.products.append(product)

    def removeProduct(self, productName):
        with self.lock:
            for product in self.products:
                if product.name == productName:
                    self.products.remove(product)
                    break

    def getProducts(self):
        with self.lock:
            return self.products.copy()





